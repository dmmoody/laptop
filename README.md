# Laptop

Laptop is a script to setup macOS laptop for web development.

It can be run multiple times on the same machine safely. It installs, upgrades, or skips packages based on what is already installed on the machine.
