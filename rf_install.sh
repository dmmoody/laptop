#!/bin/sh

red="\033[1;31m"
grn="\033[1;32m"
blu="\033[1;34m"
mag="\033[1;35m"
cyn="\033[1;36m"
white="\033[0m"

echo "\n${blu}Checking RF projects...${white}"

apps=(
  champ
  customer-portal
  geoduck
  holmes
  lenny
  mandolin
  nemo
  panda
  piranha
  pumpkin
  repaymnt_estimatr
  rf_authorize
  rf_mockserver
  shared_rubocop
  tiro
  unicorn
)

for app in "${apps[@]}"
do
  if [ -d ~/Development/${app} ]
  then
    echo "\n\t${grn}${app} already exists${white}"
  else
    echo "\n\t${mag}Cloning ${app}...${white}"
    git clone git@github.com:projectdx/${app}.git ~/Development/${app}
  fi
done

rubies=(
  2.1.9
  2.2.7
  2.3.8
  2.4.6
)

for version in "${rubies[@]}"
do
  if [ -d ~/.rubies/ruby-${version} ]
  then
    echo "\n\t${grn}Ruby ${version} is already installed${white}"
  else
    echo "\n\t${mag}Installing Ruby ${version}...${white}"
    ruby-install ruby ${version}
  fi
done
