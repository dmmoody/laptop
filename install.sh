#!/bin/sh

red="\033[1;31m"
grn="\033[1;32m"
blu="\033[1;34m"
mag="\033[1;35m"
cyn="\033[1;36m"
white="\033[0m"

echo "\n${blu}Checking homebrew...${white}"
if command -v brew > /dev/null 2>&1
then
  echo "\n\t${grn}Homebrew is already installed ${white}"
else
  echo "\n\t${mag}Installing Homebrew... ${white}"
  curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install | ruby
fi

echo "\n${blu}Checking if oh-my-zsh is installed...${white}"
if [ -d ~/.oh-my-zsh ]
then
  echo "\n\t${grn}Oh-my-zsh is already installed${white}"
else
  echo "\n\t${mag}Installing oh-my-zsh...${white}"
  curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh
fi

echo "\n${blu}Checking Dotfiles.....${white}"
if [ -d ~/Dotfiles ]
then
  echo "\n\t${grn}Dotfiles are already installed${white}"
else
  echo "\n\t${mag}Cloning dotfiles...${white}"
  git clone https://github.com/dmmoody/dotfiles.git Dotfiles
fi

echo "\n${blu}Checking Brewfile...${white}"
if [ -f ~/Brewfile ]
then
  echo "\n\t${grn}Brewfile already exists${white}"
  echo "\n${blu}Checking brew packages...${white}"
  brew bundle check &> /dev/null
  if [ $? -eq 1 ]
  then
    echo "\n\t${mag}Installing missing brew packages...${mag}"
    brew bundle install --file=~/Brewfile
  else
    echo "\n\t${grn}Brew packages are satisfied${grn}"
  fi
else
  echo "\n\t${mag}Getting Brewfile...${white}"
  curl -o ~/Brewfile https://gitlab.com/dmmoody/laptop/raw/master/Brewfile
  echo "\n\t${mag}Installing Programs...${white}"
  brew bundle --file=~/Brewfile
fi

echo "\n${blu}Checking symlinks...${white}"
symlink_files=(
  ackrc
  aliases
  gemrc
  gitconfig
  gitignore_global
  pryrc
  psqlrc
  railsrc
  rspec
  tmux.conf
  tmuxline.conf
  vimrc
  vimrc.bundles
  zshrc
)

if [ -L ~/.zshrc ]
then
  echo "\n\t${grn}zshrc is already switched to dotfile version${white}"
else
  echo "\n\t${mag}Removing original default zshrc...${white}"
  rm ~/.zshrc
fi

for i in "${symlink_files[@]}"
do
  if [ -L ~/.$i ]
  then
    echo "\n\t${grn}${i} is already symlinked${white}"
  else
    echo "\n\t${mag}Symlinking ${i}...${white}"
    ln -s ~/Dotfiles/$i ~/.$i
  fi
done

echo "\n${blu}Installing latest ruby...${white}"
rubies=(
  2.6.4
)

for version in "${rubies[@]}"
do
  if [ -d ~/.rubies/ruby-${version} ]
  then
    echo "\n\t${grn}Ruby ${version} is already installed${white}"
  else
    echo "\n\t${mag}Installing Ruby ${version}...${white}"
    ruby-install --latest ruby
    echo "2.6.4" > ~/.ruby-version
  fi
done

echo "\n${blu}Installing powerline fonts...${white}"
if [ "$(ls -A $HOME/Library/Fonts)" ]
then
  echo "\n\t${grn}Powerline fonts are already installed${white}"
else
  echo "\n\t${mag}Installing powerline fonts...${white}"
  git clone https://github.com/powerline/fonts.git
  sh fonts/install.sh
fi

echo "\n${blu}Checking necessary directories...${white}"
dirs=(
  Development
  Projects
  Experiments
  .ssh
  bin
)

for i in "${dirs[@]}"
do
  if [ -d ~/$i ]
  then
    echo "\n\t${grn}${i} already exists${white}"
  else
    echo "\n\t${mag}Creating ${i}...${white}"
    mkdir ~/$i
  fi
done

if [ -d ~/.vim/colors ]
then
  echo "\n\t${grn}.vim/colors already exists${white}"
else
  echo "\n\t${mag}Creating .vim/colors...${white}"
  mkdir -p ~/.vim/colors
  cp ~/Dotfiles/vimcolors/* ~/.vim/colors
fi

echo "\n${blu}Checking necessary files...${white}"
files=(
  .env
)

for i in "${files[@]}"
do
  if [ -f ~/$i ]
  then
    echo "\n\t${grn}${i} already exists${white}"
  else
    echo "\n\t${mag}Creating ${i}...${white}"
    touch ~/$i
  fi
done

echo "\n${blu}Setting up postgres user database${white}"

if psql -lqt | cut -d \| -f 1 | grep -qw $USER
then
  echo "\n\t${grn}Database for ${USER} already exists${white}"
else
  echo "\n\t${mag}Creating user database for ${USER}...${white}"
  createdb $USER
fi
